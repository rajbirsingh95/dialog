package com.example.dialog;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;

public class MainActivity extends AppCompatActivity {
    Button button1, button2, button3;
    LinearLayout linearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        button1 = findViewById(R.id.button3);
        button2 = findViewById(R.id.button4);
        button3 = findViewById(R.id.button5);
        linearLayout = findViewById(R.id.l1);
        button1.setOnClickListener(v -> {
            Snackbar snackbar = Snackbar.make(linearLayout, "error occur", Snackbar.LENGTH_LONG).setAction("RETRY", v1 -> Toast.makeText(MainActivity.this, "retry clicked", Toast.LENGTH_SHORT).show());
            snackbar.show();
        });

        button2.setOnClickListener((View v) -> {
            Snackbar snackbar = Snackbar.make(linearLayout, "", Snackbar.LENGTH_LONG);
            View custom = getLayoutInflater().inflate(R.layout.snackbar_custom, null);
            snackbar.getView().setBackgroundColor(Color.TRANSPARENT);
            Snackbar.SnackbarLayout snackbarLayout = (Snackbar.SnackbarLayout) snackbar.getView();
            snackbarLayout.setPadding(0, 0, 0, 0);
            snackbarLayout.addView(custom, 0);
            snackbar.show();

        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, DialogActivity.class);
                startActivity(intent);
            }
        });
    }
}