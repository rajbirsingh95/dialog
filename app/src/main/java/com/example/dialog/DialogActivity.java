package com.example.dialog;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class DialogActivity extends AppCompatActivity {
    private Button button, loading_btn, go_btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        button=findViewById(R.id.button);
        button.setOnClickListener(v -> dialogOpen());
        loading_btn = findViewById(R.id.button2);
        go_btn = findViewById(R.id.button6);
        loading_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingdialog();
            }
        });
go_btn.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        Intent intent=new Intent(DialogActivity.this,ThirdActivity.class);
        startActivity(intent);
    }
});
    }

    public void dialogOpen() {

        ExampleDialog exampleDialog = new ExampleDialog();
        exampleDialog.show(getSupportFragmentManager(), "example dialog");

    }

    public void loadingdialog() {
        Loading loading = new Loading(DialogActivity.this);
        loading.startLoading();

    }
}