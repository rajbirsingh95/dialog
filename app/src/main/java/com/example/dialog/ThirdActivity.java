package com.example.dialog;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ThirdActivity extends AppCompatActivity {
    Button button7, button8, button10,button11;
    TextView name, account, balance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        button11=findViewById(R.id.button11);
        button7 = findViewById(R.id.button7);
        button8 = findViewById(R.id.button8);
        button10 = findViewById(R.id.button10);
        name = findViewById(R.id.name);
        account = findViewById(R.id.acc);
        balance = findViewById(R.id.balance);

        button11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ThirdActivity.this,CustomDialogActivity.class);
                startActivity(intent);
            }
        });


        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                name.setText("rajbir");
            }
        });
        button8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                account.setText("1254000156894");
            }
        });
        button10.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ThirdActivity.this);
                builder.setMessage("ARE YOU SHOW ");
                builder.setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        balance.setText("50000000000");
                    }

                });
                builder.setNegativeButton("CANCEL", null);
                AlertDialog alertDialog = builder.create();
                alertDialog.show();
            }

        });

    }

    @Override
    public void onBackPressed() {
AlertDialog.Builder builder=new AlertDialog.Builder(ThirdActivity.this);
builder.setMessage("are you sure you want to quit ");
builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
    @Override
    public void onClick(DialogInterface dialog, int which) {
        ThirdActivity.super.onBackPressed();
    }
});
builder.setNegativeButton("NO",null);
AlertDialog alertDialog=builder.create();
alertDialog.show();
    }
}